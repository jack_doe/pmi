import com.ibm.websphere.management.AdminClient;
import com.ibm.websphere.management.AdminClientFactory;
import com.ibm.websphere.management.Session;
import com.ibm.websphere.management.cmdframework.CommandMgr;
import com.ibm.websphere.management.configservice.ConfigService;
import com.ibm.websphere.management.configservice.ConfigServiceProxy;
import com.ibm.websphere.pmi.stat.StatDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.ObjectName;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by defmo on 12.05.2016.
 */
public class ACS {
    public static Logger logger = LoggerFactory.getLogger(ACS.class);
    public static Properties connectProps = new Properties();
    public static AdminClient adminClient = null;
    public static CommandMgr commandMgr=null;
    public static ConfigService configService=null;
    public static Session session=null;

    public static void admClient(){
        connectProps.setProperty(AdminClient.CONNECTOR_TYPE, AdminClient.CONNECTOR_TYPE_SOAP);
        connectProps.setProperty(AdminClient.CONNECTOR_SECURITY_ENABLED,Boolean.TRUE.toString());
        connectProps.setProperty(AdminClient.CONNECTOR_SOAP_CONFIG,new File(ACS.class.getClassLoader().getResource("soap.client.props").getFile()).getAbsolutePath());
        connectProps.setProperty(AdminClient.CONNECTOR_HOST, "172.28.24.192");
        connectProps.setProperty(AdminClient.CONNECTOR_PORT, "8879");
        connectProps.setProperty(AdminClient.USERNAME, "wasadmin");
        connectProps.setProperty(AdminClient.PASSWORD, "o9p0[-]=");
        try
        {
            adminClient = AdminClientFactory.createAdminClient(connectProps);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void init() {
        try {
            commandMgr = CommandMgr.getCommandMgr(adminClient);
            configService = new ConfigServiceProxy(adminClient);
            session = new Session();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void cc()throws Exception {
        ObjectName jvmQuery;
        Object[] params;
        String[] signature;
        jvmQuery = new ObjectName("WebSphere:type=Perf,*");
        Set s = adminClient.queryNames(jvmQuery, null);
        for (Iterator j = s.iterator(); j.hasNext(); ) {
            ObjectName jvmMBean = (ObjectName) j.next();
            String type = jvmMBean.getKeyProperty("type");
            if(type != null && type.equals("Perf")) {
                System.out.println("MBean: perf =" + jvmMBean.toString());
                System.out.println ("Current statistic set: " + adminClient.invoke(jvmMBean, "getStatisticSet", null, null));
                System.out.println ("Get all statistics in PMI tree");
                signature = new String[]{"[Lcom.ibm.websphere.pmi.stat.StatDescriptor;","java.lang.Boolean"};
                params = new Object[] {new StatDescriptor[]{new StatDescriptor(null)}, new Boolean(true)};
                com.ibm.websphere.pmi.stat.WSStats[] wsStats = (com.ibm.websphere.pmi.stat.WSStats[])adminClient.invoke(jvmMBean, "getStatsArray", params, signature);
                logger.info(wsStats[0].toString());
            }
            System.out.println(jvmMBean.toString());
        }
    }
    public static void main(String Args[]){
        admClient();
        init();
        try {
            cc();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
